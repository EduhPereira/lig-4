const modalVictory = document.getElementById('modal-victory');
const closeModalVictory = document.getElementById('close-modal_victory');
const start = document.querySelector('#startGame');
const rickWIn = document.querySelector('#rickWIn');
const mortyWin = document.querySelector('#mortyWin');
const tie = document.querySelector('#tie')

//initial modal elements 
const initialModal = document.getElementById('initial-modal');
const player1 = document.getElementById('player1');
const player2 = document.getElementById('player2');
const playBtn = document.getElementById('play-btn');

//player turn indicator
let playerTurn = document.getElementById('playerTurn');

let p1 = 'Iniciar!';
let p2 = '';


closeModalVictory.addEventListener('click', () => {
    // modalVictory.classList.add('hidden');
    location.reload();
});

playBtn.addEventListener('click', () => {
    start.play() 
    start.volume = 0.5
    if (player1.value !== "" && player1.value !== undefined) {
        p1 = player1.value;
    } else {
        p1 = "Jogador 1"
    }

    if (player2.value !== "" && player2.value !== undefined) {
        p2 = player2.value;
    } else {
        p2 = "Jogador 2"
    }
    initialModal.style.display = 'none';
})

const winner = (drawBoolean, player2, player1, userNumberWinner1or2 = 1) => {
    const title = document.querySelector('.modal-victory__title');
    const content = document.querySelector('.modal-victory__content');
    const img = document.getElementById('modal-victory__image');
    if (drawBoolean) {
        tie.play()
        title.innerText = 'Draw Game!';
        content.innerText = `${player1} and ${player2} it looks like you two must restart.`;
        img.src = "./assets/images/zeus_and_rick_draws_718px.png";
    } else if (userNumberWinner1or2 === 1) {
        mortyWin.play()
        mortyWin.volume = 0.1;
        title.innerText = `${player1} wins!!!`;
        content.innerText = `What's up ${player2}?! Is that everything that you have?`;
        img.src = "./assets/images/mortyWinModalTest.png";
    } else {
        rickWIn.play()
        title.innerText = `${player2} wins!!!`;
        content.innerText = `Come on ${player1}! Where is your "super powers"?!`;
        img.src = "./assets/images/rick_punches_zeus_718px.jpg";
    }
};

const tabuleiro = document.querySelector('#tabuleiro')

const map = [
    "AAAAAAA",
    "LLLLLLL",
    "LLLLLLL",
    "LLLLLLL",
    "LLLLLLL",
    "LLLLLLL",
    "LLLLLLL",
]
const criarTabuleiro = () => {
    for (let i = 0; i < map.length; i++) {
        let criarColunas = document.createElement('div');
        criarColunas.dataset.index = i;
        criarColunas.classList.add('coluna');
        tabuleiro.appendChild(criarColunas);

        for (let j = 0; j < map[i].length; j++) {

            if (map[j][i] === 'A') {
                let criarLinhaDaBola = document.createElement('div');
                criarLinhaDaBola.dataset.index = j;
                criarLinhaDaBola.setAttribute('id', 'bolaDaVez' + i);
                criarLinhaDaBola.classList.add('bolaDaVez');
                criarColunas.appendChild(criarLinhaDaBola);
            }

            if (map[j][i] === 'L') {
                let criarLinhas = document.createElement('div');
                criarLinhas.dataset.index = j;
                criarLinhas.setAttribute('id', 'bolaDaVez' + i + j);
                criarLinhas.classList.add('linha');
                criarColunas.appendChild(criarLinhas);
            }
        }
    }
}

criarTabuleiro()

columnQtd = { 'bolaDaVez0': 6, 'bolaDaVez1': 6, 'bolaDaVez2': 6, 'bolaDaVez3': 6, 'bolaDaVez4': 6, 'bolaDaVez5': 6, 'bolaDaVez6': 6 }
let player = "Jogador 1";
playerTurn.innerHTML = p1;

const spawnBall = (e) => {
    if (columnQtd[e.target.id] > 0) {
        if (player === "Jogador 1") {
            ball = document.createElement('div');
            ball.classList.add('ball');
            ball.setAttribute('id', 'jogador1');
            player = "Jogador 2";
            playerTurn.innerHTML = p2;
        } else {
            ball = document.createElement('div');
            ball.classList.add('ball2');
            ball.setAttribute('id', 'jogador2');
            player = "Jogador 1";
            playerTurn.innerHTML = p1;
        }


        position = document.getElementById(e.target.id + columnQtd[e.target.id]);
        position.appendChild(ball);

        columnQtd[e.target.id]--;
        verificarVitoriaVertical()
        verificarVitoriaHorizontal()
        verificarVitoriaDiagonalEsquerda()
        verificarVitoriaDiagonalDireita()
    }

}

column1 = document.getElementById('bolaDaVez0');
column1.addEventListener('click', spawnBall);

column2 = document.getElementById('bolaDaVez1');
column2.addEventListener('click', spawnBall);

column3 = document.getElementById('bolaDaVez2');
column3.addEventListener('click', spawnBall);

column4 = document.getElementById('bolaDaVez3');
column4.addEventListener('click', spawnBall);

column5 = document.getElementById('bolaDaVez4');
column5.addEventListener('click', spawnBall);

column6 = document.getElementById('bolaDaVez5');
column6.addEventListener('click', spawnBall);

column7 = document.getElementById('bolaDaVez6');
column7.addEventListener('click', spawnBall);

const verificarVitoriaVertical = () => {

    for (let i = 0; i < tabuleiro.children.length; i++) {

        for (let j = 3; j >= 0; j--) {

            if (tabuleiro.children[i].children[j].innerHTML !== '' &&
                tabuleiro.children[i].children[j + 1].innerHTML !== '' &&
                tabuleiro.children[i].children[j + 2].innerHTML !== '' &&
                tabuleiro.children[i].children[j + 3].innerHTML !== '') {


                if (tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i].children[j + 1].children[0].className &&
                    tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i].children[j + 2].children[0].className &&
                    tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i].children[j + 3].children[0].className) {
                    winnerNumber = Number(player[player.length - 1]);
                    draw = 0;
                    winner(draw, p1, p2, winnerNumber);
                    modalVictory.classList.remove('hidden');
                }

            }
            if (i === 0) {
                if (tabuleiro.children[i].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 1].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 2].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 3].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 4].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 5].children[1].childElementCount !== 0 &&
                    tabuleiro.children[i + 6].children[1].childElementCount !== 0) {


                    draw = 1;
                    winner(draw, p1, p2);
                    modalVictory.classList.remove('hidden');
                }
            }
        }
    }
}


const verificarVitoriaHorizontal = () => {
    for (let i = 0; i < tabuleiro.children.length; i++) {

        for (let j = 6; j >= 3; j--) {

            if (tabuleiro.children[i].children[j].innerHTML !== '') {
                if (i !== 4 && i !== 5 && i !== 6) {
                    if (tabuleiro.children[i + 1].children[j].innerHTML !== '' &&
                        tabuleiro.children[i + 2].children[j].innerHTML !== '' &&
                        tabuleiro.children[i + 3].children[j].innerHTML !== '') {
                        if (tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 1].children[j].children[0].className &&
                            tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 2].children[j].children[0].className &&
                            tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 3].children[j].children[0].className) {
                            winnerNumber = Number(player[player.length - 1]);
                            draw = 0;
                            winner(draw, p1, p2, winnerNumber);
                            modalVictory.classList.remove('hidden');
                        }
                    }
                }
                if (i === 0) {
                    if (tabuleiro.children[i].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 1].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 2].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 3].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 4].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 5].children[1].childElementCount !== 0 &&
                        tabuleiro.children[i + 6].children[1].childElementCount !== 0) {


                        draw = 1;
                        winner(draw, p1, p2);
                        modalVictory.classList.remove('hidden');
                    }
                }
            }
        }
    }
}



const verificarVitoriaDiagonalEsquerda = () => {
    for (let i = 0; i < tabuleiro.children.length; i++) {
        for (let j = 6; j > 4; j--) {
            if (i !== 4 && i !== 5 && i !== 6) {
                if (tabuleiro.children[i].children[j].innerHTML !== '' &&
                    tabuleiro.children[i + 1].children[j - 1].innerHTML !== '' &&
                    tabuleiro.children[i + 2].children[j - 2].innerHTML !== '' &&
                    tabuleiro.children[i + 3].children[j - 3].innerHTML !== '') {

                    if (tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 1].children[j - 1].children[0].className &&
                        tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 2].children[j - 2].children[0].className &&
                        tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i + 3].children[j - 3].children[0].className) {
                        winnerNumber = Number(player[player.length - 1]);
                        draw = 0;
                        winner(draw, p1, p2, winnerNumber);
                        modalVictory.classList.remove('hidden');
                    }
                }
            }
        }
    }
}

const verificarVitoriaDiagonalDireita = () => {
    for (let i = 6; i >= 0; i--) {
        for (let j = 6; j > 0; j--) {
            if (i !== 0 && i !== 1 && i !== 2) {
                if (tabuleiro.children[i].children[j].innerHTML !== '' &&
                    tabuleiro.children[i - 1].children[j - 1].innerHTML !== '' &&
                    tabuleiro.children[i - 2].children[j - 2].innerHTML !== '' &&
                    tabuleiro.children[i - 3].children[j - 3].innerHTML !== '') {

                    if (tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i - 1].children[j - 1].children[0].className &&
                        tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i - 2].children[j - 2].children[0].className &&
                        tabuleiro.children[i].children[j].children[0].className === tabuleiro.children[i - 3].children[j - 3].children[0].className) {
                        winnerNumber = Number(player[player.length - 1]);
                        draw = 0;
                        winner(draw, p1, p2, winnerNumber);
                        modalVictory.classList.remove('hidden');
                    }
                }
            }
        }
    }
}